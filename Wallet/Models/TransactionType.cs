﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    /// <summary>
    /// описание типов платежей 
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// платеж
        /// </summary>
        Payment = 0,
        /// <summary>
        /// обмен валюты внутри кошелька
        /// </summary>
        Exchange = 1,
        /// <summary>
        /// вывод средств
        /// </summary>
        Withdraval = 2,
        /// <summary>
        /// пополнение счёта
        /// </summary>
        Refill = 3
    }
}
