﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    public class WalletTransaction
    {
        public TransactionType TransactionType { get; set; }
        /// <summary>
        /// сумма вылюты транзакции
        /// </summary>
        public decimal Amount { get; set; }
        public int TransactionCurrency { get; set; }
        public int? CurrencyTo { get; set; }
        public int WalletId { get; set; } 
    }
}
