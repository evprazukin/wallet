﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    /// <summary>
    /// класс для получения курса
    /// </summary>
    public class CurrencyRateFilter
    {
        /// <summary>
        /// валюта для котрой получается курс
        /// </summary>
        public int? CurrencyIsoCodeFrom { get; set; }
        /// <summary>
        /// валютя из котороый курс, если пустое, то это рубль
        /// </summary>
        public int? CurrencyIsoCodeTo { get; set; }

    }
}
