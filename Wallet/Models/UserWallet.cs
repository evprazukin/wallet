﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    public class UserWallet
    {
        public int WalletId { get; set; }
        public int UserId { get; set; }
        public List<CurrencyBalance> CurrencyBalances { get; set; }
    }
}
