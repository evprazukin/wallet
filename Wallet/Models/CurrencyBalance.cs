﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    public class CurrencyBalance
    {
        /// <summary>
        /// 
        /// </summary>
        public int IsoCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Balance { get; set; }
    }
}
