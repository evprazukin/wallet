﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallet.Api.Models
{
    public class Users
    {
        public int? UserId { get; set; }
        public string UserName { get; set; }
    }
}
