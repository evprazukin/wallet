﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Api.Models;

namespace Wallet.Api.Services.Interfaces
{
    public interface IWalletService
    {
        UserWallet GetUserWallet(int userId);
        UserWallet UpdateUserWallet(UserWallet userWallet);
    }
}
