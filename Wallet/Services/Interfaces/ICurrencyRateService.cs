﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Wallet.Api.Models;
using WebRatesServiceReference;

namespace Wallet.Api.Services.Interfaces
{
    public interface ICurrencyRateService
    {
       Task<decimal> GetCurrencyRate(CurrencyRateFilter currencyRateFilter);
       Task<List<ServiceCurrencyRate>> GetCurrencyRates();
    }
}