﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Api.Data;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Services
{
    public class WalletService:IWalletService
    {
        private readonly UsersWalletsData _data = UsersWalletsData.GetUsersWallets();
        public UserWallet GetUserWallet(int userId)
        {
            var wallet = _data.Wallets.FirstOrDefault(x => x.UserId == userId);
            return wallet;
        }

        public UserWallet UpdateUserWallet(UserWallet userWallet)
        {
            throw new Exception($"Операция не поддерживается!");
        }
    }
}
