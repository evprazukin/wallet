﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Api.Data;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Services
{
    public class UserService: IUserService
    {
        private readonly UsersWalletsData _data = UsersWalletsData.GetUsersWallets();
        public async Task<List<Users>> GetUsers(Users usersFilter)
        {
            var users = _data.Users.Where(x =>
                (usersFilter.UserId == null || x.UserId == usersFilter.UserId)
              &&(string.IsNullOrEmpty(usersFilter.UserName) || x.UserName == usersFilter.UserName)
                ).ToList();
            return users;
        }
    }
}
