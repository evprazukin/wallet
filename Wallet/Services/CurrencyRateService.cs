﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;
using WebRatesServiceReference;

namespace Wallet.Api.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CurrencyRateService: ICurrencyRateService
    {
        CbServiceClient webServiceClient = new CbServiceClient();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<decimal> GetCurrencyRate(CurrencyRateFilter currencyRateFilter)
        {
            var rate = 0;
            var rates = await webServiceClient.GetCurrencyRatesAsync();

            decimal rateFromValue = 1;
            decimal rateToValue = 1;
            if (currencyRateFilter.CurrencyIsoCodeFrom != null)
            {
                var rateFrom = rates.FirstOrDefault(x => x.IsoCode == currencyRateFilter.CurrencyIsoCodeFrom);
                rateFromValue = (decimal)rateFrom?.Rate / (decimal)rateFrom?.FaceValue;
            }
            if (currencyRateFilter.CurrencyIsoCodeTo != null)
            {
                var rateTo = rates.FirstOrDefault(x => x.IsoCode == currencyRateFilter.CurrencyIsoCodeTo);
                rateToValue = (decimal)rateTo?.Rate / (decimal)rateTo?.FaceValue;
            }
            //var rateTo = currencyRateFilter.CurrencyIsoCodeTo == null ? 1 : (decimal)rates.FirstOrDefault(x => x.IsoCode == currencyRateFilter.CurrencyIsoCodeTo).Rate;

            var crossRate = rateFromValue / rateToValue;
            return crossRate;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<ServiceCurrencyRate>> GetCurrencyRates()
        {
            var rates = await webServiceClient.GetCurrencyRatesAsync();
            return rates.ToList();
        }
    }
}
