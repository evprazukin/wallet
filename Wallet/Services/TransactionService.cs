﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Api.Data;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Services
{
    /// <summary>
    /// сервис по проведению транзакций по типу
    /// 
    /// </summary>
    public class TransactionService : ITransactionService
    {
        private readonly UsersWalletsData _data = UsersWalletsData.GetUsersWallets();
        private readonly ICurrencyRateService _currencyRateService;
        public TransactionService(ICurrencyRateService currencyRateService)
        {
            _currencyRateService = currencyRateService;
        }
        public async Task<bool> AddTransaction(WalletTransaction walletTransaction)
        {
            var userWallet = _data.Wallets.FirstOrDefault(x => x.WalletId == walletTransaction.WalletId);
            if (userWallet == null) throw new Exception($"Кошелёк с id {walletTransaction.WalletId} не найден!");
            if (walletTransaction.Amount < 0) throw new Exception($"Сумма должна быть положительной!");
            switch (walletTransaction.TransactionType)
                {
                    case TransactionType.Payment:
                    throw new Exception($"Операция не поддерживается!");
                case TransactionType.Exchange:
                        return await CreateExchangeTransaction(walletTransaction, userWallet);
                    case TransactionType.Refill:
                        return CreateRefillTransaction(walletTransaction, userWallet);
                    case TransactionType.Withdraval:
                        return CreateWithdrawalTransaction(walletTransaction, userWallet);
                }
            
            return false;
        }

        private async Task<bool> CreateExchangeTransaction(WalletTransaction walletTransaction, UserWallet userWallet)
        {
            
                var currencyRateFilter = new CurrencyRateFilter
                {
                    CurrencyIsoCodeFrom = walletTransaction.TransactionCurrency,
                    CurrencyIsoCodeTo = walletTransaction.CurrencyTo
                };
                var currentRate = await _currencyRateService.GetCurrencyRate(currencyRateFilter);
                var transactionAmount = walletTransaction.Amount * currentRate;
                var currencyFromBalance =
                    userWallet.CurrencyBalances.FirstOrDefault(x => x.IsoCode == walletTransaction.TransactionCurrency);
                
                if (currencyFromBalance == null || currencyFromBalance.Balance < walletTransaction.Amount) 
                    throw new Exception($"Валюта транзакции не найдена или недостаточно средств!");
                if (walletTransaction.CurrencyTo == null) throw new Exception($"Валюта перевода не найдена!");
                
                var currencyToBalance = userWallet.CurrencyBalances.FirstOrDefault(x => x.IsoCode == walletTransaction.CurrencyTo);
                //если такой валюты нет, то добавим
                if (currencyToBalance == null)
                {
                    var newCurrency = new CurrencyBalance
                    {
                        IsoCode = (int)walletTransaction.CurrencyTo,
                        Balance = transactionAmount
                    };
                    userWallet.CurrencyBalances.Add(newCurrency);
                }
                else
                {
                    currencyToBalance.Balance += transactionAmount;
                }

                currencyFromBalance.Balance -= walletTransaction.Amount;
                
                return true;
        }
        private bool CreateRefillTransaction(WalletTransaction walletTransaction, UserWallet userWallet)
        {
            var currencyBalance = userWallet.CurrencyBalances.FirstOrDefault(x=>x.IsoCode ==  walletTransaction.TransactionCurrency);
            if (currencyBalance == null)
            {
                var newCurrency = new CurrencyBalance
                {
                    IsoCode = (int)walletTransaction.TransactionCurrency,
                    Balance = walletTransaction.Amount
                };
                userWallet.CurrencyBalances.Add(newCurrency);
            }
            else
            {
              currencyBalance.Balance += walletTransaction.Amount;
            }

           
            return true;
        }
        private bool CreateWithdrawalTransaction(WalletTransaction walletTransaction, UserWallet userWallet)
        {
            var currencyBalance = userWallet.CurrencyBalances.FirstOrDefault(x => x.IsoCode == walletTransaction.TransactionCurrency);
            if (currencyBalance == null || currencyBalance.Balance < walletTransaction.Amount) throw new Exception($"Валюта транзакции не найдена или недостаточно средств!");
            currencyBalance.Balance -= walletTransaction.Amount;
            return true;
        }
    }
}
