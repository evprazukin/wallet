﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Api.Models;

namespace Wallet.Api.Data
{
    public class UsersWalletsData
    {
        private static UsersWalletsData _instance;
        public static UsersWalletsData GetUsersWallets()
        {
            return _instance ?? (_instance = new UsersWalletsData());
        }
        public List<Users> Users { get; set; }
        public List<UserWallet> Wallets { get; set; }
        private UsersWalletsData()
        {
            Users = new List<Users>
            {
                new Users{UserId = 0,UserName = "Ivan"},
                new Users{UserId = 1,UserName = "Peter"},
                new Users{UserId = 2,UserName = "Jon"}
            };
            Wallets = new List<UserWallet>
            {
                new UserWallet
                {
                    UserId = 0,
                    WalletId = 0,
                    CurrencyBalances = new List<CurrencyBalance>
                    {
                        new CurrencyBalance{Balance = 100,IsoCode = 840},
                        new CurrencyBalance{Balance = 100,IsoCode = 4217}
                    }
                },
                new UserWallet
                    {
                        UserId = 1,
                        WalletId = 1,
                        CurrencyBalances = new List<CurrencyBalance>
                        {
                            new CurrencyBalance{Balance = 100,IsoCode = 980},
                            new CurrencyBalance{Balance = 100,IsoCode = 4217}
                        }
                    },
                new UserWallet
                {
                    UserId = 2,
                    WalletId = 2,
                    CurrencyBalances = new List<CurrencyBalance>
                    {
                        new CurrencyBalance{Balance = 100,IsoCode = 970},
                        new CurrencyBalance{Balance = 100,IsoCode = 4217}
                    }
                }
            };
        }
    }
}
