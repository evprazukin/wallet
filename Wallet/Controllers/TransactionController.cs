﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly string _successfulTransaction = "транзакция удалась";
        private readonly string _unSuccessfulTransaction = "транзакция не удалась";
        private readonly ITransactionService _transactionService;
        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }
        [HttpPost]
        public async Task<JsonResult> Post(WalletTransaction transaction)
        {
            var res = await  _transactionService.AddTransaction(transaction);
            if (res) return new JsonResult(_successfulTransaction);
            return new JsonResult(_unSuccessfulTransaction);
        }
    }
}
