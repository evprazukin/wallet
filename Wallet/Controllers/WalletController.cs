﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        private readonly IWalletService _walletService;

        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }

        [HttpGet]
        public JsonResult Get(int userId)
        {
            var wallet = _walletService.GetUserWallet(userId);
            return new JsonResult(wallet);
        }

        [HttpPost]
        public JsonResult Post(UserWallet newWallet)
        {
            var addedWallet = _walletService.UpdateUserWallet(newWallet);
            return new JsonResult(addedWallet);
        }

        [HttpPut]
        public JsonResult Put(UserWallet wallet)
        {
            var updatedWallet = _walletService.UpdateUserWallet(wallet);
            return new JsonResult(updatedWallet);
        }
    }
}
