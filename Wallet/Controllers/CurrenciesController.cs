﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wallet.Api.Models;
using Wallet.Api.Services;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Controllers
{
   /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
   {
       private readonly ICurrencyRateService _currencyRateService;

       public CurrenciesController(ICurrencyRateService currencyRateService)
       {
           _currencyRateService = currencyRateService;
       }
       [HttpGet]
       public async Task<JsonResult> Get()
       {
           var res = await _currencyRateService.GetCurrencyRates();
           return new JsonResult(res);
       }
    }
}
