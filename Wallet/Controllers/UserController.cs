﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wallet.Api.Models;
using Wallet.Api.Services.Interfaces;

namespace Wallet.Api.Controllers
{
    /// <summary>
    /// получение пользователей
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _usersServise;

        public UserController(IUserService usersServise)
        {
            _usersServise = usersServise;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Get()
        {
            var users = _usersServise.GetUsers(new Users());
            return new JsonResult(users);
        }
    }
}
