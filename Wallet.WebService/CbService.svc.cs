﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Wallet.WebService.CbRates;


namespace Wallet.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CbService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CbService.svc or CbService.svc.cs at the Solution Explorer and start debugging.
    public class CbService : ICbService
    {
        public ServiceCurrencyRate[] GetCurrencyRates()
        {
            return CbRatesServiceClient.GetCurrencyRates();
        }

        public ServiceCurrencyRate[] GetCurrencyRatesByDate(DateTime date)
        {
            return CbRatesServiceClient.GetCurrencyRatesByDate(date);
        }
    }
}
