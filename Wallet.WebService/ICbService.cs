﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Wallet.WebService.CbRates;

namespace Wallet.WebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICbService" in both code and config file together.
    [ServiceContract]
    public interface ICbService
    {

        [OperationContract]
        ServiceCurrencyRate[] GetCurrencyRates();
        [OperationContract]
        ServiceCurrencyRate[] GetCurrencyRatesByDate(DateTime date);
    }
}
