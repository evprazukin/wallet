﻿using System.Data;
using System.Linq;
using Wallet.WebService.CbRates;

namespace Wallet.WebService.Extensions
{
    public static class DataSetExtensions
    {
        public static ServiceCurrencyRate[] ToArrayOfRates(this DataSet ds)
        {
            var count = ds.Tables[0].AsEnumerable().Count();
            var arrayOfRates = ds.Tables[0].AsEnumerable()
                .Select(dataRow => new ServiceCurrencyRate
                {
                    Name = dataRow.Field<string>("Vname").TrimEnd(),
                    FaceValue = dataRow.Field<decimal>("Vnom"),
                    Rate = dataRow.Field<decimal>("Vcurs"),
                    IsoCode = dataRow.Field<int>("Vcode"),
                    StringCode = dataRow.Field<string>("VchCode"),
                }).ToList();
            //так как ЦБ РФ не присылает рубль, то добавим его
            arrayOfRates.Add(new ServiceCurrencyRate
            {
                Name = "Российсий рубль",
                FaceValue = 1,
                IsoCode = 4217,
                Rate = 1,
                StringCode = "RUB"
            });
            return arrayOfRates.ToArray();
        }
    }
}