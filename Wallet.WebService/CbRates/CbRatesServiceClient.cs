﻿using System;
using Wallet.WebService.Extensions;
using Wallet.WebService.ServiceReference1;

namespace Wallet.WebService.CbRates
{
    public class CbRatesServiceClient
    {
        public static ServiceCurrencyRate[] GetCurrencyRatesByDate(DateTime date)
        {
            using (var soapClient = new DailyInfoSoapClient())
            {
                var dailyRate = soapClient.GetCursOnDate(date.Date);
                return dailyRate.ToArrayOfRates();
            }

       
        }

        public static ServiceCurrencyRate[] GetCurrencyRates()
        {
            using (var soapClient = new DailyInfoSoapClient())
            {
                var dailyRate = soapClient.GetCursOnDate(DateTime.Today);
                return dailyRate.ToArrayOfRates();
            }
        }
    }
}