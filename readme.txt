

Актуальный курс берётся с сайта ЦБ РФ.
Данные для проверки созданны в виде синглтона класса UsersWalletsData,
так как было решено не создавать БД и подключать репозиторий для небольшого тестового задания.
Добавлен Сваггер для удобства отправки запросов к АПИ.

получить список валют с курсами CurrenciesController
получить список пользователей UserController
получить данные кошелька по id пользователя WalletController
операции c кошельком TransactionController

тестовые кейсы для кошелька с id = 1
поменять 10 гривен внутри кошелька на рубли
{
  "transactionType": 1,
  "amount": 10,
  "transactionCurrency": 980,
  "currencyTo": 4217,
  "walletId": 1
}
поменять 80 гривен внутри кошелька на доллары
{
  "transactionType": 1,
  "amount": 80,
  "transactionCurrency": 980,
  "currencyTo": 840,
  "walletId": 1
}
пополнить кошелёк на 80 гривен
{
  "transactionType": 3,
  "amount": 80,
  "transactionCurrency": 980,
  "walletId": 1
}
снять 10 гривен
{
  "transactionType": 2,
  "amount": 10,
  "transactionCurrency": 980,
  "walletId": 1
}